# LionsCAL

LionsCAL is a small tool which provides a quick and easy way to add all the year's events to your calendar, in one go.

## For Dublin Lions Club members:

**The only file you need to use is the .ics for the upcoming Lions year**. For example dublinLions_2017-2018.ics for 2017/2018.

It's an iCal format, which is a standard format for calendars. In most of the cases, you just need to open the file with the software you use for your calendar (Apple Calendar, Microsoft Outlook...)

If you use Google Calendar (the web application), you'll need to go to "parameters" and then "import a calendar"...

## For other people who would like to use this work:

The others files (xlsx, json) are there for those who would like to create their own files (other Lions Clubs for example).

Once the .json file is made, just run lionsCal.php in order to create the .ics file.

## Author

Cyrille Georgel < cyrille.georgel@gmail.com >

Feel free to ask if you need any help.

## Dublin Lions Club (Ireland)

Dublin Lions Club is a service organisation which has been serving the elderly, youth and disadvantaged for over 60 years in Dublin.

If you want to know more about Dublin Lions Club, please visit our website : http://www.dublinlionsclub.ie/.

You can also follow us on Twitter, https://twitter.com/dublinlionsclub.
