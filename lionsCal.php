<?php

/*
@Author: Cyrille Georgel <cgeorgel>
@Date:   2016-07-14T19:21:57+01:00
@Email:  cyrille.georgel@gmail.com
@Project: Project
# @Last modified by:   cgeorgel
# @Last modified time: 2017-06-21T22:47:27+01:00
*/

error_reporting(E_ALL);
ini_set("display_errors", 1);


function getNameFormat()
{
    /*
    Lions year starts the 1st of July
    For example:
    dublinLions_2016-2017.json -> dublinLions_2016-2017.ics
    */

    $dateFrom = new DateTime();
    $dateTo = new DateTime('+1 year');

    $nameFormat = 'dublinLions_' . $dateFrom->format('Y') . '-' . $dateTo->format('Y');

    return $nameFormat;
}


function randKey($lenght = 24)
{
    /*
    returns a simple random key
    */

	$randKey = '';

	$chars = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
	'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2',
	'3', '4', '5', '6', '7', '8', '9');

	for($i=0; $i<$lenght; $i++)
	{
		$randKey .= $chars[rand(0, count($chars)-1)];
	}

	return $randKey;
}


# get the events for the year from the json file:

$nameFormat = getNameFormat();

$file = $nameFormat . '.json';
$handle = fopen($file, 'r');
$dataset = fread($handle, filesize($file));
$events = json_decode($dataset);
#print_r($events);


# put all the events in ical format:

$vCal = '';

$vCal .= 'BEGIN:VCALENDAR' . "\n";
$vCal .= 'VERSION:2.0' . "\n";
$vCal .= 'PRODID:-//BlueRock Ltd Cyrille//Dublin Lions//EN' . "\n";

foreach($events as $event)
{
    $dtstamp = date('Ymd') . 'T' . date('His');

    $dtstart = strtotime($event->dtstart);
    $dtstart = date('Ymd', $dtstart) . 'T' . date('His', $dtstart);

    $dtend = strtotime($event->dtend);
    $dtend = date('Ymd', $dtend) . 'T' . date('His', $dtend);

    $uid = $dtstamp . '-'  . 'dublin-lions' . '-'  . randKey(4) . '@cyrille.me';

    $vCal .= 'BEGIN:VEVENT' . "\n";
    $vCal .= 'UID:' . $uid . "\n";
    $vCal .= 'DTSTAMP:' . $dtstamp . "\n";
    $vCal .= 'DTSTART:' . $dtstart . "\n";
    $vCal .= 'DTEND:' . $dtend . "\n";
    $vCal .= 'SUMMARY:'. $event->summary . "\n";
    $vCal .= 'DESCRIPTION:'. $event->description . "\n";
    $vCal .= 'END:VEVENT' . "\n";

}

$vCal .= 'END:VCALENDAR' . "\n";

print $vCal;

# write the ical format to file

$icsFile = file_put_contents($nameFormat . '.ics', $vCal);

?>
